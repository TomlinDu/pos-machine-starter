package pos.machine;

import java.security.PublicKey;
import java.util.*;

import static pos.machine.ItemsLoader.loadAllItems;

public class PosMachine {
    public String printReceipt(List<String> barcodes) {
        String receipt = "";
        for(int i = 0;i < barcodes.size();i++){
            if(isValid(barcodes.get(i))) {
                continue;
            }else{
                barcodes.remove(i);
            }
        }
        receipt = generateReceipt(barcodes);
        return receipt;
    }

    public boolean isValid(String code) {
        boolean flag = false;
        List<Item> itemList = loadAllItems();
        for(int i = 0;i<itemList.size();i++){
            if(code == itemList.get(i).getBarcode())
                flag = true;
        }
        return flag;
    }

    public String generateReceipt(List<String> barcodes){
        String receipt = "";

        List<ReceiptItem> receiptItems = generateReceiptItems(barcodes);

        String lines = generateLines(receiptItems);

        int totalprice = computeTotalprice(receiptItems);

        receipt = "***<store earning no money>Receipt***\n" +
                lines +
                "----------------------\n" +
                "Total: " + totalprice + " (yuan)\n" +
                "**********************";

        return receipt;
    }

    private int computeTotalprice(List<ReceiptItem> receiptItems) {
        int totalprice = 0;
        for(int i = 0;i < receiptItems.size();i++){
            totalprice += receiptItems.get(i).getSubtotal();
        }
        return totalprice;
    }

    public List<ReceiptItem> generateReceiptItems(List<String> barcodes){
        List<Item> itemList = loadAllItems();

        List<ReceiptItem> receiptItems = new ArrayList<>();

        Map<String, Integer> quantityMap = new HashMap<>();

        barcodes.forEach(barcode->{
            quantityMap.put(barcode,quantityMap.getOrDefault(barcode,0)+1);
        });

        itemList.forEach(item->{
            ReceiptItem receiptItem = new ReceiptItem();
            receiptItem.setName(item.getName());
            receiptItem.setUnitPrice(item.getPrice());
            receiptItem.setQuantity(quantityMap.get(item.getBarcode()));
            receiptItem.setSubtotal(item.getPrice()*quantityMap.get(item.getBarcode()));
            receiptItems.add(receiptItem);
        });

        return receiptItems;
    }

    public  String generateLines(List<ReceiptItem> receiptItems){
        String lines = new String();
        for(int i = 0;i < receiptItems.size();i++){
            ReceiptItem receiptItem=receiptItems.get(i);
            String itemLine="Name: "+receiptItem.getName()+", Quantity: "+receiptItem.getQuantity()+", Unit price: "+receiptItem.getUnitprice()+" (yuan), Subtotal: "+receiptItem.getSubtotal()+" (yuan)\n";
            lines += itemLine;
        }
        return lines;
    }



}
