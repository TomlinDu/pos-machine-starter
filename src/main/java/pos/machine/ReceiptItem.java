package pos.machine;

public class ReceiptItem {
    private String name;
    private int quantity;
    private int unitprice;
    private int subtotal;

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public int getQuantity() {
        return quantity;
    }
    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }


    public int getUnitprice(){ return unitprice; }
    public void setUnitPrice(int unitPrice) {
        this.unitprice = unitPrice;
    }

    public int getSubtotal(){
        return subtotal;
    }
    public void setSubtotal(int subtotal) {
        this.subtotal = subtotal;
    }


}